package org.karate.chop;

import static org.junit.Assert.assertEquals;
import static org.karate.chop.KarateChop.chop;

import org.junit.Test;

public class KarateChopTest {
    /*
    Ideally we should have only one assertion by test method
    */

    @Test
    public void shouldReturnMinusOneIfItemsIsEmpty(){
        assertEquals(-1, chop(3, new int[0]));
    }

    @Test
    public void shouldReturnMinusOneIfItemNotFound(){
        assertEquals(-1, chop(3, new int[]{1}));
        assertEquals(-1, chop(0,  new int[]{1, 3, 5}));
        assertEquals(-1, chop(2,  new int[]{1, 3, 5}));
        assertEquals(-1, chop(4,  new int[]{1, 3, 5}));
        assertEquals(-1, chop(6,  new int[]{1, 3, 5}));

        assertEquals(-1, chop(0, new int[]{1, 3, 5}));
        assertEquals(-1, chop(2, new int[]{1, 3, 5}));
        assertEquals(-1, chop(4, new int[]{1, 3, 5}));
        assertEquals(-1, chop(6, new int[]{1, 3, 5}));

        assertEquals(-1, chop(0, new int[]{1, 3, 5, 7}));
        assertEquals(-1, chop(2, new int[]{1, 3, 5, 7}));
        assertEquals(-1, chop(4, new int[]{1, 3, 5, 7}));
        assertEquals(-1, chop(6, new int[]{1, 3, 5, 7}));
    }
    
    @Test
    public void shouldReturnTheIndexOfWantedValue(){
        assertEquals(0,  chop(1, new int[]{1}));
        assertEquals(0,  chop(1, new int[]{1, 3, 5}));
        assertEquals(1,  chop(3, new int[]{1, 3, 5}));
        assertEquals(2,  chop(5, new int[]{1, 3, 5}));
        assertEquals(0,  chop(1, new int[]{1, 3, 5, 7}));
        assertEquals(1,  chop(3, new int[]{1, 3, 5, 7}));
        assertEquals(2,  chop(5, new int[]{1, 3, 5, 7}));
        assertEquals(3,  chop(7, new int[]{1, 3, 5, 7}));
    }

}

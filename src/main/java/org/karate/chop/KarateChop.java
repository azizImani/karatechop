package org.karate.chop;

public class KarateChop {

    public static int chop(int wantedValue, int[] items) {
        return recursiveBinarySearch(wantedValue, items, 0, items.length);
    }

    private static int recursiveBinarySearch(int wantedValue, int[] items, int start, int end) {

        if (start < end) {
            //Using this instruction instead of 'middle = start + end' in order to avoid overflow
            int middle = start + (end - start) / 2;
            if (wantedValue < items[middle]) {
                return recursiveBinarySearch(wantedValue, items, start, middle);
            }
            if (wantedValue > items[middle]) {
                return recursiveBinarySearch(wantedValue, items, middle + 1, end);
            } else {
                return middle;
            }
        }
        return -1;
    }
}
